# Reproduction Package for "A Unifying Approach for Control-Flow-Based Loop Abstraction"

## Requirements

For running the experiments in this artifact,
we require an installation of Ubuntu 20.04 with the following
software packages installed:

- ant
- benchexec
- gcc
- make
- openjdk-11-jdk-headless
- unzip

In general, for running the tools from SV-COMP 2022,
the required packages for an Ubuntu 20.04 installation are documented here:
https://gitlab.com/sosy-lab/benchmarking/competition-scripts/-/blob/svcomp22/test/Dockerfile.user.2022


The following commands should suffice on Ubuntu 20.04:
```
sudo apt install ant gcc openjdk-11-jdk-headless make unzip
wget https://github.com/sosy-lab/benchexec/releases/download/3.11/benchexec_3.11-2_all.deb
sudo dpkg -i benchexec_3.11-2_all.deb
sudo adduser $USER benchexec
sudo reboot
```

In the following, unless otherwise mentioned, we will assume that the current directory
is the directory where this `README.md` file is in.

In case the artifact VM is used, this will be the folder `~/loop-abstraction`.
Also, the VM comes with all requirements preinstalled.
The artifact VM is built using the Makefile targets `artifact.ova` or `standalone-artifact.ova`.
The later is optimized for size and does not require to load files from the internet,
hence the network adapter was removed. Since we use vagrant for VM creation, the username
and password will both be `vagrant`.

## How to Configure the Benchmarks in General

In order to run benchmarks, one usually wants control over the benchmarking parameters,
for example to control where result files are written, which resource limits are used etc.
For this we provide some configuration scripts. The default configuration is
called `config.sh` and shall be sourced before (once per terminal session is enough)
running the experiments:

```
$ . ./config.sh
```

This will export some environment variables that will be used by the Makefile to
control how the benchmark command is called.
This config file also contains extensive documentation on what each option is good for.
For example, to control the directory in which the benchmark results will be stored,
you can adjust the value in the variable `RESULTSDIR`.
The variable BENCHMARKOPTIONS is used to specify a file with options for benchexec.
This way, you can also adapt the resource limits for the benchmarks, which
is especially useful if you want to or have to run the benchmarks with lower resource limits
than we did in our experiments.

For example, you can reduce the resource limits to 1 CPU, 45 seconds CPU time and 4000MB
of RAM by running the following:

```
cat benchmarkOptions_local > myOptions
echo "--limitCores=1" >> myOptions
echo "--memorylimit=4000MB" >> myOptions
echo "--timelimit=45s" >> myOptions
export BENCHMARKOPTIONS="myOptions"
```

This will create a new file with benchmark options named `myOptions` based on `benchmarkOptions_local`
that will in addition have the three options from the listing that will make benchexec override
the resource limits that are specified in the benchmark definition files. The last command
will set the environment variable BENCHMARKOPTIONS to point to that file.

## How to run Loop Abstraction experiments using CPAchecker on ReachSafety-Loops tasks

```
make loopsummary-test
```

The benchmark results will be stored in the folder `results/` by default.
Along a folder with the result files, there should also be files with extension `.xml.bz2`,
which contain all the benchmark results for the 6 different experiments that were executed
(BMC, value analysis, and predicate analysis, each with and without loop abstraction).

To generate a comparison table, you can run `table-generator` like outlined in
the listing below. We will use the files from our archived experiment results in
the folder `archived-results`, If you want to do the same for your
own, reproduced benchmark results, you can just replace the folder name with `results`
or whatever folder you stored your reproduced results in.

```
benchexec/bin/table-generator -n loopabstraction-comparison \
archived-results/loopsummary-test.*.results.{bmc,loopsummary-bmc,predicate,loopsummary-predicate,value,loopsummary-value}.ReachSafety-Loops.xml.bz2
```

The command above will generate result tables, which can be viewed in a browser, as well as csv tables
for evaluation in an automated manner. Here is an example of what the above command should print out:

```
INFO:     archived-results/loopsummary-test.2022-07-04_07-25-30.results.bmc.ReachSafety-Loops.xml.bz2
INFO:     archived-results/loopsummary-test.2022-07-04_07-25-30.results.loopsummary-bmc.ReachSafety-Loops.xml.bz2
INFO:     archived-results/loopsummary-test.2022-07-04_07-25-30.results.predicate.ReachSafety-Loops.xml.bz2
INFO:     archived-results/loopsummary-test.2022-07-04_07-25-30.results.loopsummary-predicate.ReachSafety-Loops.xml.bz2
INFO:     archived-results/loopsummary-test.2022-07-04_07-25-30.results.value.ReachSafety-Loops.xml.bz2
INFO:     archived-results/loopsummary-test.2022-07-04_07-25-30.results.loopsummary-value.ReachSafety-Loops.xml.bz2
INFO: Merging results...
INFO: Generating table...
INFO: Writing HTML into archived-results/loopabstraction-comparison.table.html ...
INFO: Writing CSV  into archived-results/loopabstraction-comparison.table.csv ...
INFO: Writing HTML into archived-results/loopabstraction-comparison.diff.html ...
INFO: Writing CSV  into archived-results/loopabstraction-comparison.diff.csv ...
INFO: done
```

**Notes:** The HTML table that ends in `diff.html` shows only tasks where the verdicts
for some of the configurations tested differ. In the tables you can also look at the
output that a tool generated for each run. For that, you simply click on the run,
but for this to work you need to unzip the corresponding log files first, which
are located in a zip file in the results directory next to the benchmark result xml files.

## How to Run Experiments on Abstracted Tasks

The tasks that showed improvement in the loop abstraction experiments above where already
copied into the folders `plain` and `abstracted`. The later contains versions of the tasks
where the loop abstraction that helped us solve the task were applied.
There is currently no automation for this step, so if you want reproduce this experiment
step you would need to execute the experiments from the previous section, look at the
result tables, filter the files accordingly and compile them into the folders `plain` and
`abstracted` yourself.

To run CBMC, Ultimate Automizer, and Symbiotic against these plain and abstracted tasks,
you can use the following Makefile targets:

```
make cbmc-experiments
make symbiotic-experiments
make automizer-experiments
```

This will run the aforementioned tools first on the plain tasks, then on the abstracted tasks.

For viewing the results, you can use `table-generator` again, here an example on our archived
experimental data:

```
for tool in cbmc automizer symbiotic 
do
  benchexec/bin/table-generator -n ${tool}-comparison archived-results/${tool}.*.results.exp.{plain,abstracted}.xml.bz2
done
```

This should generate an output similar to the following, listing all the 3 html tables generated:

```
INFO:     archived-results/cbmc.2022-07-04_05-14-55.results.exp.plain.xml.bz2
INFO:     archived-results/cbmc.2022-07-04_05-29-40.results.exp.abstracted.xml.bz2
INFO: Merging results...
INFO: Generating table...
INFO: Writing HTML into archived-results/cbmc-comparison.table.html ...
INFO: Writing CSV  into archived-results/cbmc-comparison.table.csv ...
INFO: Writing HTML into archived-results/cbmc-comparison.diff.html ...
INFO: Writing CSV  into archived-results/cbmc-comparison.diff.csv ...
INFO: done
INFO:     archived-results/automizer.2022-07-04_05-19-57.results.exp.plain.xml.bz2
INFO:     archived-results/automizer.2022-07-04_05-36-05.results.exp.abstracted.xml.bz2
INFO: Merging results...
INFO: Generating table...
INFO: Writing HTML into archived-results/automizer-comparison.table.html ...
INFO: Writing CSV  into archived-results/automizer-comparison.table.csv ...
INFO: Writing HTML into archived-results/automizer-comparison.diff.html ...
INFO: Writing CSV  into archived-results/automizer-comparison.diff.csv ...
INFO: done
INFO:     archived-results/symbiotic.2022-07-04_05-16-30.results.exp.plain.xml.bz2
INFO:     archived-results/symbiotic.2022-07-04_05-32-42.results.exp.abstracted.xml.bz2
INFO: Merging results...
INFO: Generating table...
INFO: Writing HTML into archived-results/symbiotic-comparison.table.html ...
INFO: Writing CSV  into archived-results/symbiotic-comparison.table.csv ...
INFO: Writing HTML into archived-results/symbiotic-comparison.diff.html ...
INFO: Writing CSV  into archived-results/symbiotic-comparison.diff.csv ...
INFO: done
```
