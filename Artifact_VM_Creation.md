# Artifact VM Creation

## Working with Vagrant

This document describes how to use Vagrant to easily create an artifact VM
from the contents of this repository.

Vagrant is a tool that makes it easy to create VMs (or docker containers)
based on a simple descriptive file, which is usually named `Vagrantfile`.

In a directory with such a file, it is enough to run the following command:

```
vagrant up
```

This will download the necessary images and perform the setup steps
that are specified in the vagrant file, and run the created VM.

For the `Vagrantfile` in this repository, we specify VirtualBox as
VM provider, therefore you need to have virtualbox installed on your
system. If you want to use another hypervisor, you will have to adjust
the provider settings on your own.

After running `vagrant up`, you can connect to the newly created VM via:

```
vagrant ssh
```

The following command can be used to halt the VM:

```
vagrant halt
```

Finally, you can also at any point delete the VM again:

```
vagrant destroy
```

You can adapt the resource settings of the created VM in the `Vagrantfile`
by editing the following part:

```
  config.vm.provider "virtualbox" do |vb|
    vb.gui = true # change this to false if you want a headless VM
    vb.cpus = "2" # change this for a different number of vCPUs on the VM
    vb.memory = "4096" # change this to adapt the RAM available to the VM (in MB)
    vb.name = "loop-abstraction-vm" # change this to use a different name for the VM
  end
```

This can come in handy in case you want to speed running the experiments in the artifact.
In cases where your host system does not have that many resources to spare,
you can also reduce the resource requirements this way.

## Exporting the VM using Virtualbox


You can use Virtualbox to export the artifact VM.
This is also possible on command line via:

```
vboxmanage export loop-abstraction-vm -o artifact.ova
```

There is also a target in the Makefile that will do everything
from creating the artifact VM to exporting it into a file
named artifact.ova. In order to do this just run:

```
make artifact.ova
```

## Creating a Self-Contained Artifact

By default, the artifact will not contain any information
that is already available in other artifacts,
like benchmark tasks and tool archives.

The idea is that these are downloaded on demand.
However, it is desirable to have a version of the artifact
that is truly self-contained. Since this will require
downloading large amounts of tools and benchmark files,
it will require some additional steps to reduce the size
of the resulting artifact VM.
For example, zip files are deleted after they are unpacked,
and sv-benchmarks are sparsely checked out to only contain
the files that are needed for the experiments. `.git` folders
are deleted, as these still contain the entire repository information
and take up a large amount of space as well.
Finally, the VM disk image is converted from vmdk to vdi, unused disk
space that was previously filled with data is zeroed out and the
vdi disk image is then shrunk again.

We provide an additional Makefile target that takes care of all
those steps:

```
make standalone-artifact.ova
```

This requires qemu-nbd and nbdfuse to be installed for accessing
and shrinking the disk images from the host system:

```
apt install qemu-utils nbdfuse
```
