#!/bin/bash
set -e
trap "exit" INT

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
if [[ ! -v BENCHMARKCOMMAND ]]; then
  echo "sourcing config $SCRIPT_DIR/config.sh"
  . $SCRIPT_DIR/config.sh
fi

if [[ -z "$1" ]]; then
  echo "Please provide the tool name to execute as parameter!"
  exit 1
fi

TOOL=$1

OPTIONS=""
if [[ ! -z "$2" ]]; then
	OPTIONS=@$SCRIPT_DIR/$2
fi

cd $TOOL
rm -rf exp
cp -r ../plain exp
$SCRIPT_DIR/$BENCHMARKCOMMAND --tool-directory . exp/$TOOL.xml @$SCRIPT_DIR/$BENCHMARKOPTIONS -o  $SCRIPT_DIR/$RESULTSDIR $OPTIONS

rm -rf exp
cp -r ../abstracted exp
$SCRIPT_DIR/$BENCHMARKCOMMAND --tool-directory .  exp/$TOOL.xml @$SCRIPT_DIR/$BENCHMARKOPTIONS -o $SCRIPT_DIR/$RESULTSDIR $OPTIONS
