SHELL=/usr/bin/bash

configured:
	./check_configured.sh

benchexec:
	git clone --depth 1 --branch 3.11 https://github.com/sosy-lab/benchexec.git

sv-benchmarks:
	git clone --depth 1 --branch svcomp22 https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks.git

cpachecker:
	git clone --branch loopsummary https://github.com/sosy-lab/cpachecker
	git -C cpachecker checkout 9820cdf77abde40679846a29715ca510dd736e01 # commit on loopsummary branch
	cd cpachecker && ant jar

compact: sv-benchmarks
	if [ -e sv-benchmarks/.git ] ; then \
	  git -C sv-benchmarks sparse-checkout init --cone; \
	  git -C sv-benchmarks sparse-checkout set c/{loops,loop-acceleration,loop-crafted,loop-invgen,loop-lit,loop-new,loop-industry-pattern,loops-crafted-1,loop-invariants,loop-simple,loop-zilu,verifythis,nla-digbench,nla-digbench-scaling,properties}; \
	  rm -rf sv-benchmarks/.git; \
	fi
	rm -rf cpachecker/.git
	rm -rf cbmc.zip symbiotic.zip uautomizer.zip

downloads: benchexec sv-benchmarks cbmc symbiotic automizer cpachecker

cbmc:
	if [ ! -f cbmc.zip ] ; then \
	  wget "https://gitlab.com/sosy-lab/sv-comp/archives-2022/-/raw/main/2022/cbmc.zip"; \
	fi
	unzip cbmc.zip

symbiotic:
	if [ ! -f symbiotic.zip ] ; then \
	  wget "https://gitlab.com/sosy-lab/sv-comp/archives-2022/-/raw/main/2022/symbiotic.zip"; \
	fi
	unzip symbiotic.zip

automizer:
	if [ ! -f uautomizer.zip ] ; then \
	  wget "https://gitlab.com/sosy-lab/sv-comp/archives-2022/-/raw/main/2022/uautomizer.zip"; \
	fi
	unzip uautomizer.zip
	mv UAutomizer-linux automizer

cbmc-experiments: cbmc benchexec configured
	./run_tool.sh cbmc ${BENCHMARKOPTIONS3RDPARTY}

symbiotic-experiments: symbiotic benchexec configured
	./run_tool.sh symbiotic ${BENCHMARKOPTIONS3RDPARTY}

automizer-experiments: automizer benchexec configured
	./run_tool.sh automizer ${BENCHMARKOPTIONS3RDPARTY}

loopsummary-test: cpachecker sv-benchmarks benchexec configured
	${BENCHMARKCOMMAND} --tool-directory cpachecker loopsummary-test.xml @${BENCHMARKOPTIONS} -o $(RESULTSDIR)

artifact.ova: Vagrantfile
	vagrant destroy -f
	vagrant up
	vagrant halt
	vboxmanage sharedfolder remove loop-abstraction-vm --name vagrant
	rm -f artifact.ova
	vboxmanage export loop-abstraction-vm -o artifact.ova
	vagrant destroy -f

standalone-artifact.ova: Vagrantfile compact.sh Makefile
	vagrant destroy -f
	vagrant up
	vagrant provision --provision-with standalone # additional step to artifact.ova
	vagrant halt
	vboxmanage sharedfolder remove loop-abstraction-vm --name vagrant
	vboxmanage modifyvm "loop-abstraction-vm" --nic1 none # get rid of network interface
	rm -f standalone-artifact.ova
	./compact.sh
	vboxmanage export loop-abstraction-vm -o standalone-artifact.ova
	vagrant destroy -f

.PHONY:loopsummary-test cmbc-experiments automizer-experiments symbiotic-experiments configured compact
