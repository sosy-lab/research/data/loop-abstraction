#!/bin/bash

for requiredoption in BENCHMARKCOMMAND BENCHMARKOPTIONS RESULTSDIR
do
  if [ -z "${!requiredoption}" ]
  then
    >&2 echo -e "$requiredoption not set! You need to source a config first, for example by runnning:\n\n. config.sh\n"
    exit 1
  fi
done
