#!/bin/bash

#######################################################################
# Configuration file for running experiments with benchexec
# all environment variables exported here will be interpreted as paths
# that are relative to the directory of the Makefile.
#######################################################################


# benchmark command to use (at SoSy-Lab there is a different command
# with which the benchmarks can be executed on a cluster):
export BENCHMARKCOMMAND=benchexec/bin/benchexec

# file containing paramters for calling benchexec, such as filesystem
# adjustments for containerization and resource limits.
# If you want to adjust the result limits, you can add/adjust the
# following options in this file:
# --limitCores=<n>
# --memorylimit=<m>MB
# --timelimit=<t>s
# depending on your filesystem layout, you might have to fiddle
# around with some of benchexec's options to find a way this
# works for your setup. The default values in this config
# should work on most systems.
# benchexec will give you suggestions what to do in case it does not
# work, try changing benchexec's view on your filesystem by
# specifiying different mount points with some of the following
# options:
# --overlayfs, --hidden-dir, --read-only-dir, --full-access-dir
export BENCHMARKOPTIONS=benchmarkOptions_local

# this is the prefix for where the benchmark results are stored by
# benchexec. Add a trailing slash to make benchexec store the results
# in this directory:
export RESULTSDIR=results/
