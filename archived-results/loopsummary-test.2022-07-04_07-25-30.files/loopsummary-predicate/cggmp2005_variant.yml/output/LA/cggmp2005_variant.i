extern void abort(void);
extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();
extern char __VERIFIER_nondet_char();
extern double __VERIFIER_nondet_double();
extern float __VERIFIER_nondet_float();
extern unsigned long __VERIFIER_nondet_ulong();
extern unsigned long long __VERIFIER_nondet_ulonglong();
extern unsigned int __VERIFIER_nondet_uint();
extern int __VERIFIER_nondet_int();
extern void abort(void);

extern void __assert_fail (const char *__assertion, const char *__file,
      unsigned int __line, const char *__function)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
extern void __assert_perror_fail (int __errnum, const char *__file,
      unsigned int __line, const char *__function)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
extern void __assert (const char *__assertion, const char *__file, int __line)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));

void reach_error() { ((void) sizeof ((0) ? 1 : 0), __extension__ ({ if (0) ; else __assert_fail ("0", "assert.h", 3, __extension__ __PRETTY_FUNCTION__); })); }
extern void abort(void);
void assume_abort_if_not(int cond) {
  if(!cond) {abort();}
}
void __VERIFIER_assert(int cond) {
  if (!(cond)) {
      ERROR: {reach_error();abort();}
  }
  return;
}
int __VERIFIER_nondet_int();
int main() {
    int lo, mid, hi;
    lo = 0;
    mid = __VERIFIER_nondet_int();
    if (!(mid > 0 && mid <= 1000000)) return 0;
    hi = 2*mid;
    // START LOOPCONSTANTEXTRAPOLATION
    if (mid > (0)) {
      long long int mid__VERIFIER_LA_tmp0;
      mid__VERIFIER_LA_tmp0 = mid;
      long long int __VERIFIER_LA_iterations0;
      __VERIFIER_LA_iterations0 = ((mid__VERIFIER_LA_tmp0 - 0) / 1L) + 1L;
      int lo__VERIFIER_LA_old_tmp0;
      lo__VERIFIER_LA_old_tmp0 = lo;
      lo = (__VERIFIER_LA_iterations0 * 1L) + lo__VERIFIER_LA_old_tmp0;
      int hi__VERIFIER_LA_old_tmp0;
      hi__VERIFIER_LA_old_tmp0 = hi;
      hi = (__VERIFIER_LA_iterations0 * -1L) + hi__VERIFIER_LA_old_tmp0;
      int mid__VERIFIER_LA_old_tmp0;
      mid__VERIFIER_LA_old_tmp0 = mid;
      mid = (__VERIFIER_LA_iterations0 * -1L) + mid__VERIFIER_LA_old_tmp0;
    }
    // END LOOPCONSTANTEXTRAPOLATION
    __VERIFIER_assert(lo == hi);
    return 0;
}
