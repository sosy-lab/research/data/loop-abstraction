extern void abort(void);
extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();
extern char __VERIFIER_nondet_char();
extern double __VERIFIER_nondet_double();
extern float __VERIFIER_nondet_float();
extern unsigned long __VERIFIER_nondet_ulong();
extern unsigned long long __VERIFIER_nondet_ulonglong();
extern unsigned int __VERIFIER_nondet_uint();
extern int __VERIFIER_nondet_int();
extern void abort(void);
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "count_up_down-2.c", 3, "reach_error"); }

void __VERIFIER_assert(int cond) {
  if (!(cond)) {
    ERROR: {reach_error();abort();}
  }
  return;
}
unsigned int __VERIFIER_nondet_uint();

int main()
{
  unsigned int n = __VERIFIER_nondet_uint();
  unsigned int x=n, y=0;
  // START LOOPCONSTANTEXTRAPOLATION
  if (x > (0)) {
    long long int x__VERIFIER_LA_tmp0;
    x__VERIFIER_LA_tmp0 = x;
    long long int __VERIFIER_LA_iterations0;
    __VERIFIER_LA_iterations0 = ((x__VERIFIER_LA_tmp0 - 0) / 1L) + 1L;
    unsigned int x__VERIFIER_LA_old_tmp0;
    x__VERIFIER_LA_old_tmp0 = x;
    x = (__VERIFIER_LA_iterations0 * -1L) + x__VERIFIER_LA_old_tmp0;
    unsigned int y__VERIFIER_LA_old_tmp0;
    y__VERIFIER_LA_old_tmp0 = y;
    y = (__VERIFIER_LA_iterations0 * 1L) + y__VERIFIER_LA_old_tmp0;
  }
  // END LOOPCONSTANTEXTRAPOLATION
  __VERIFIER_assert(y!=n);
}
