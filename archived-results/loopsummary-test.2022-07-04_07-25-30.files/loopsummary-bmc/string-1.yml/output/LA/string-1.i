extern void abort(void);
extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();
extern char __VERIFIER_nondet_char();
extern double __VERIFIER_nondet_double();
extern float __VERIFIER_nondet_float();
extern unsigned long __VERIFIER_nondet_ulong();
extern unsigned long long __VERIFIER_nondet_ulonglong();
extern unsigned int __VERIFIER_nondet_uint();
extern int __VERIFIER_nondet_int();
extern void abort(void);

extern void __assert_fail (const char *__assertion, const char *__file,
      unsigned int __line, const char *__function)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
extern void __assert_perror_fail (int __errnum, const char *__file,
      unsigned int __line, const char *__function)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
extern void __assert (const char *__assertion, const char *__file, int __line)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));

void reach_error() { ((void) sizeof ((0) ? 1 : 0), __extension__ ({ if (0) ; else __assert_fail ("0", "string-1.c", 3, __extension__ __PRETTY_FUNCTION__); })); }

extern void abort(void);
void assume_abort_if_not(int cond) {
  if(!cond) {abort();}
}
void __VERIFIER_assert(int cond) {
  if (!(cond)) {
    ERROR: {reach_error();abort();}
  }
  return;
}


extern char __VERIFIER_nondet_char();

int main()
{
  char string_A[5], string_B[5];
  int i, j, nc_A, nc_B, found=0;

  for(i=0; i<5; i++)
    string_A[i]=__VERIFIER_nondet_char();
  if (!(string_A[5 -1]=='\0')) return 0;

  for(i=0; i<5; i++)
    string_B[i]=__VERIFIER_nondet_char();
  if (!(string_B[5 -1]=='\0')) return 0;

  nc_A = 0;
  // START HAVOCSTRATEGY
  if ((string_A[nc_A]) != ('\x0')) {
  nc_A = __VERIFIER_nondet_int();
  }
  if ((string_A[nc_A]) != ('\x0')) abort();
  // END HAVOCSTRATEGY

  nc_B = 0;
  // START HAVOCSTRATEGY
  if ((string_B[nc_B]) != ('\x0')) {
  nc_B = __VERIFIER_nondet_int();
  }
  if ((string_B[nc_B]) != ('\x0')) abort();
  // END HAVOCSTRATEGY

  if (!(nc_B >= nc_A)) return 0;


  i=j=0;
  // START HAVOCSTRATEGY
  if ((j < nc_B) & (i < nc_A)) {
  i = __VERIFIER_nondet_int();
  j = __VERIFIER_nondet_int();
  }
  if ((j < nc_B) & (i < nc_A)) abort();
  // END HAVOCSTRATEGY

  found = (j>nc_B-1);

  __VERIFIER_assert(found == 0 || found == 1);

  return 0;
}
