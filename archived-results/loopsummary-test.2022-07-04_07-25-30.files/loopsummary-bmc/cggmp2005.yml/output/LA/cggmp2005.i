extern void abort(void);
extern int __VERIFIER_nondet_int();
extern _Bool __VERIFIER_nondet_bool();
extern char __VERIFIER_nondet_char();
extern double __VERIFIER_nondet_double();
extern float __VERIFIER_nondet_float();
extern unsigned long __VERIFIER_nondet_ulong();
extern unsigned long long __VERIFIER_nondet_ulonglong();
extern unsigned int __VERIFIER_nondet_uint();
extern int __VERIFIER_nondet_int();
extern void abort(void);

extern void __assert_fail (const char *__assertion, const char *__file,
      unsigned int __line, const char *__function)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
extern void __assert_perror_fail (int __errnum, const char *__file,
      unsigned int __line, const char *__function)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
extern void __assert (const char *__assertion, const char *__file, int __line)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));

void reach_error() { ((void) sizeof ((0) ? 1 : 0), __extension__ ({ if (0) ; else __assert_fail ("0", "assert.h", 3, __extension__ __PRETTY_FUNCTION__); })); }
extern void abort(void);
void assume_abort_if_not(int cond) {
  if(!cond) {abort();}
}
void __VERIFIER_assert(int cond) {
  if (!(cond)) {
      ERROR: {reach_error();abort();}
  }
  return;
}
int __VERIFIER_nondet_int();
int main() {
    int i,j;
    i = 1;
    j = 10;
    // START LOOPCONSTANTEXTRAPOLATION
    if (j >= i) {
      long long int j__VERIFIER_LA_tmp0;
      j__VERIFIER_LA_tmp0 = j;
      long long int i__VERIFIER_LA_tmp0;
      i__VERIFIER_LA_tmp0 = i;
      long long int __VERIFIER_LA_iterations0;
      __VERIFIER_LA_iterations0 = (j__VERIFIER_LA_tmp0 - i__VERIFIER_LA_tmp0) / 3L;
      int j__VERIFIER_LA_old_tmp0;
      j__VERIFIER_LA_old_tmp0 = j;
      j = (__VERIFIER_LA_iterations0 * -1L) + j__VERIFIER_LA_old_tmp0;
      int i__VERIFIER_LA_old_tmp0;
      i__VERIFIER_LA_old_tmp0 = i;
      i = (__VERIFIER_LA_iterations0 * 2L) + i__VERIFIER_LA_old_tmp0;
    }
    // END LOOPCONSTANTEXTRAPOLATION
    __VERIFIER_assert(j == 6);
    return 0;
}
