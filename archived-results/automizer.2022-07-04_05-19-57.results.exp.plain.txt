   BENCHMARK INFORMATION
benchmark definition:    exp/automizer.xml
name:                    automizer
run sets:                exp.plain
date:                    Mon, 2022-07-04 05:19:57 CEST
tool:                    ULTIMATE Automizer 0.2.2-839c364b
tool executable:         ./Ultimate.py
options:                 --full-output
resource limits:
- memory:                15000.0 MB
- time:                  900 s
- cpu cores:             2
hardware requirements:
- cpu model:             1230
- cpu cores:             2
- memory:                15000.0 MB
------------------------------------------------------------



exp.plain
Run set 1 of 1 with options '--full-output' and propertyfile 'None'

inputfile                      status                       cpu time   wall time        host
--------------------------------------------------------------------------------------------
benchmark02_linear.yml         true                            10.30        5.90  apollon105
benchmark11_linear.yml         true                            12.25        6.95  apollon079
benchmark25_linear.yml         true                            11.54        6.66  apollon014
benchmark26_linear.yml         true                            11.30        6.73  apollon105
benchmark43_conjunctive.yml    true                            13.25        7.53  apollon076
const_1-2.yml                  TIMEOUT                        960.25      942.84  apollon079
nested3-1.yml                  TIMEOUT                        960.25      939.70  apollon108
nested3-2.yml                  TIMEOUT                        960.30      932.79  apollon111
simple_1-1.yml                 TIMEOUT                        960.24      943.65  apollon076
simple_1-2.yml                 true                            12.39        7.16  apollon108
simple_2-1.yml                 true                            10.09        5.87  apollon048
simple_4-1.yml                 TIMEOUT                        960.36      940.72  apollon108
simple_4-2.yml                 true                            12.41        7.43  apollon079
sum_by_3.yml                   TIMEOUT                        960.24      946.85  apollon073
terminator_02-2.yml            true                            11.41        6.36  apollon079
terminator_03-2.yml            true                            11.30        6.41  apollon076
trex03-2.yml                   true                            11.28        6.49  apollon076
trex04.yml                     true                            11.93        6.73  apollon108
--------------------------------------------------------------------------------------------
Run set 1                      done                             None      957.87           -

Statistics:             18 Files
  correct:              12
    correct true:       12
    correct false:       0
  incorrect:             0
    incorrect true:      0
    incorrect false:     0
  unknown:               6
  Score:                24 (max: 32)
